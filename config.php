<?php
/**
 * Configuração de conexão ao MySQL.
 *
 * Arquivo de configuração de conexão ao banco de dados.
 *
 * @package projetoReservas
 *
 * @author Jonathan T.
 *
 * @param $dbname - Nome da tabela
 * @param $server - IP do servidor
 * @param $dbuser - Usuário DB
 * @param $dbpass - Senha DB
 * @param $charset - colação de caracteres
 * @param $dsn - Monta string de conexão
 * @param $pdo - Realiza a conexão
 * @param $e - Captura o erro de conexão vindo do PDOException

 **/
require 'environment.php';

$config = [];

if(ENVIRONMENT == 'development'){
    define("BASE_URL", "http://192.168.0.17/twitter/");
    $config['dbname']  = 'twitter';
    $config['host']    = 'localhost';
    $config['dbuser']  = 'root';
    $config['dbpass']  = 'Adminuser2@172';
    $config['charset'] = 'utf8';

}elseif(ENVIRONMENT == 'production'){
    define("BASE_URL", "https://twitter.jonathantolotti.com.br/");
    $config['dbname']  = 'jonatha4_twitter';
    $config['host']    = 'jonathantolotti.com.br';
    $config['dbuser']  = 'jonatha4_root';
    $config['dbpass']  = 'camille1311';
    $config['charset'] = 'utf8';
}else{
    define("BASE_URL", "https://twitter.jonathantolotti.com.br/maint.php");
    header("Location:".BASE_URL);
}


global $db;
try{
    $dsn = "mysql:dbname=".$config['dbname'].";"."host=".$config['host'].";"."charset=".$config['charset'].";";
    $db = new PDO($dsn, $config['dbuser'], $config['dbpass']);

}catch(PDOException $e){
    echo "ERRO:".$e->getMessage();
    exit;
}