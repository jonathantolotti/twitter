<?php
class Usuarios extends Model{
    private $uid;
    public function __construct($id = ''){
        parent::__construct();
        if(!empty($id)){
            $this->uid = $id;
        }
    }
    public function isLogged(){
        if(isset($_SESSION['twlg']) && !empty($_SESSION['twlg'])){
            return true;
        }else{
            return false;
        }
    }
    public function usuarioExiste($usuario){
        $sql = "SELECT usuario FROM usuarios WHERE usuario = '$usuario'";
        $sql = $this->db->query($sql);
        if($sql->rowCount() > 0){
            return true;
        }else{
            return false;
        }
    }
    public function usuarioValido($id){
        $sql = "SELECT id FROM usuarios WHERE id = '$id'";
        $sql = $this->db->query($sql);
        if($sql->rowCount() > 0){
            return true;
        }else{
            return false;
        }
    }
    public function inserirUsuario($nome, $email, $senha, $usuario, $foto, $bio){
        $tmpName = md5(time() . rand(0, 999999)) . '.jpg';
        move_uploaded_file($foto['tmp_name'], 'assets/images/users/' . $tmpName);
        $sql = "INSERT INTO usuarios SET nome = '$nome', email = '$email', senha = '$senha', usuario = '$usuario', foto = '$tmpName', datacad = NOW(), bio = '$bio'";
        $sql = $this->db->query($sql);
        $id = $this->db->lastInsertId();
        return $id;
    }
    public function fazerLogin($usuario, $senha){
        $sql = "SELECT * FROM usuarios WHERE usuario = '$usuario' AND senha = '$senha'";
        $sql = $this->db->query($sql);
        if($sql->rowCount() > 0){
            $sql = $sql->fetch();
            $_SESSION['twlg'] = $sql['id'];
            return true;
        }else{
            $_SESSION['tentativas']++;
            return false;
        }
    }
    public function getNome(){
        if(!empty($this->uid)){
            $sql = "SELECT id,usuario, bio, foto, verificado FROM usuarios WHERE id = '{$this->uid}'";
            $sql = $this->db->query($sql);
            if($sql->rowCount() > 0){
                $sql = $sql->fetch();
                return $sql;
            }
        }
    }
    public function getNomeUsuarioEmail($email){
        $sql = "SELECT usuario FROM usuarios WHERE email = '$email'";
        $sql = $this->db->query($sql);
        if($sql->rowCount() > 0){
            $sql = $sql->fetch();
            return $sql;
        }
    }
    public function countSeguido(){
        $sql = "SELECT id_seguidor FROM relacionamentos WHERE id_seguidor = '{$this->uid}'";
        $sql = $this->db->query($sql);
        return $sql->rowCount();
    }
    public function countSeguidores(){
        $sql = "SELECT id_seguido FROM relacionamentos WHERE id_seguido = '{$this->uid}'";
        $sql = $this->db->query($sql);
        return $sql->rowCount();
    }
    public function getUsuarios($limit){
        $dados = array();
        $sql = "SELECT id, usuario,
        (SELECT count(*) FROM relacionamentos r WHERE r.id_seguidor = '{$this->uid}' AND r.id_seguido = usuarios.id) as seguido
        FROM usuarios WHERE id != '{$this->uid}' 
        AND (SELECT count(*) FROM relacionamentos r WHERE r.id_seguidor = '{$this->uid}' AND r.id_seguido = usuarios.id) != 1
        ORDER BY datacad DESC LIMIT $limit";
        $sql = $this->db->query($sql);
        if($sql->rowCount() > 0){
            $dados = $sql->fetchAll();
        }
        return $dados;
    }
    public function getSeguidos(){
        $dados = array();
        $sql = "SELECT id_seguido FROM relacionamentos WHERE id_seguidor = '{$this->uid}'";
        $sql = $this->db->query($sql);
        if($sql->rowCount() > 0){
            foreach($sql->fetchAll() as $seg){
                $dados[] = $seg['id_seguido'];
            }
        }
        return $dados;
    }
    public function getNomeUsuario($id){
        if(!empty($id)){
            $sql = "SELECT id,nome,usuario, bio, foto, verificado FROM usuarios WHERE id = '$id'";
            $sql = $this->db->query($sql);
            if($sql->rowCount() > 0){
                $sql = $sql->fetch();
                return $sql;
            }
        }
    }
    public function countSeguidoUsuario($id){
        $sql = "SELECT id_seguidor FROM relacionamentos WHERE id_seguidor = '$id'";
        $sql = $this->db->query($sql);
        return $sql->rowCount();
    }
    public function countSeguidoresUsuario($id){
        $sql = "SELECT id_seguido FROM relacionamentos WHERE id_seguido = '$id'";
        $sql = $this->db->query($sql);
        return $sql->rowCount();
    }
    public function getUsuariosPerfil($id, $limit){
        $dados = array();
        $sql = "SELECT id, usuario,
        (SELECT count(*) FROM relacionamentos r WHERE r.id_seguidor = '$id' AND r.id_seguido = usuarios.id) as seguido
        FROM usuarios WHERE id != '$id' 
        AND (SELECT count(*) FROM relacionamentos r WHERE r.id_seguidor = '$id' AND r.id_seguido = usuarios.id) != 1
        LIMIT $limit";
        $sql = $this->db->query($sql);
        if($sql->rowCount() > 0){
            $dados = $sql->fetchAll();
        }
        return $dados;
    }
    public function verSeguidores($id){
        $current = $_SESSION['twlg'];
        $dados = array();
        $sql = "SELECT usuarios.id, usuarios.usuario,
                (SELECT count(*) FROM relacionamentos r WHERE r.id_seguidor = '$current' AND r.id_seguido = usuarios.id) as seguido
                FROM relacionamentos 
                LEFT JOIN usuarios ON relacionamentos.id_seguidor = usuarios.id WHERE id_seguido = '$id' AND usuarios.id != '$current'";
        $sql = $this->db->query($sql);
        if($sql->rowCount() > 0){
            $dados = $sql->fetchAll();
        }
        return $dados;
    }
    public function verQuemSegue($id){
        $current = $_SESSION['twlg'];
        $dados = array();
        $sql = "SELECT usuarios.id, usuarios.usuario, 
                (SELECT count(*) FROM relacionamentos r WHERE r.id_seguidor = '$current' AND r.id_seguido = usuarios.id) as seguido 
                FROM relacionamentos LEFT JOIN usuarios ON relacionamentos.id_seguido = usuarios.id 
                WHERE id_seguidor = '$id' AND usuarios.id != '$current'";
        $sql = $this->db->query($sql);
        if($sql->rowCount() > 0){
            $dados = $sql->fetchAll();
        }
        return $dados;
    }
    public function validaFollow($id_user, $id_seguir){
        $sql = "SELECT id FROM relacionamentos WHERE id_seguidor = '$id_user' AND id_seguido = '$id_seguir'";
        $sql = $this->db->query($sql);
        if($sql->rowCount() > 0){
            return false;
        }else{
            return true;
        }
    }
    public function validaEmail($email){
        $sql = "SELECT id FROM usuarios WHERE email = '$email'";
        $sql = $this->db->query($sql);
        if($sql->rowCount() > 0){
            return true;
        }else{
            return false;
        }
    }
    public function alterarUsuario($id, $nome, $usuario, $bio, $tmpName){
        if(!empty($id) && !empty($nome) && !empty($usuario) && !empty($bio)){
            $sql = "UPDATE usuarios SET nome = '$nome', usuario = '$usuario', bio = '$bio', foto = '$tmpName'
                    WHERE id = '$id'";
            $this->db->query($sql);
        }
    }
    public function apagaTweet($id){
        $idTweet = $id;
        $idUser = $_SESSION['twlg'];
        $sql = "DELETE FROM posts WHERE id = '$idTweet' AND id_usuario = '$idUser'";
        $sql = $this->db->query($sql);
    }
    public function gerarTokenSenha($email, $token){
        if(!empty($email) && !empty($token)){
            $data_expiracao = date('Y-m-d H:i', strtotime('+1 day'));
            $sql = "INSERT INTO senha_token (email, token, data_expiracao) 
                    VALUES ('$email', '$token', '$data_expiracao')";
            $sql = $this->db->query($sql);
        }
    }
    public function validaToken($token,$email){
        $sql = "SELECT email FROM senha_token WHERE token = '$token' AND email = '$email' AND used = 0 AND data_expiracao > NOW()";
        $sql = $this->db->query($sql);
        if($sql->rowCount() > 0){
            return true;
        }else{
            return false;
        }
    }
    public function invalidaToken($token){
        $sql = "UPDATE senha_token SET used = 1 WHERE token = '$token'";
        $sql = $this->db->query($sql);
    }
    public function alteraSenha($senha,$email){
        if(!empty($senha) && !empty($email)){
            $sql = "UPDATE usuarios SET senha = '$senha' WHERE email = '$email'";
            $sql = $this->db->query($sql);
            return true;
        }else{
            return false;
        }
    }

}