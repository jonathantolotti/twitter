<?php
class Post extends Model{

    public function publicar($msg){
        $id_usuario = $_SESSION['twlg'];

        $sql = "INSERT INTO posts SET id_usuario = '$id_usuario', data_post = NOW(), mensagem = '$msg'";
        $sql = $this->db->query($sql);
    }
    public function getFeed($lista, $limit){
        $feed = array();
        if(count($lista) > 0){
            $sql = "SELECT *, 
                    (SELECT usuario FROM usuarios WHERE usuarios.id = posts.id_usuario) as usuario,
                    (SELECT COUNT(*) FROM curtida WHERE curtida.id_mensagem = posts.id)as curtidas 
                    FROM posts WHERE id_usuario IN (".implode(',', $lista).") 
                    ORDER BY data_post DESC LIMIT ".$limit;
            $sql = $this->db->query($sql);
            if($sql->rowCount() > 0){
                $feed = $sql->fetchAll();
            }
        }
        return $feed;
    }
    public function totalPublicacoes($id){
            $sql = "SELECT id_usuario FROM posts WHERE id_usuario = '$id'";
            $sql = $this->db->query($sql);
            return $sql->rowCount();
    }
    public function getFeedUsuario($id){
        $feed = array();
            $sql = "SELECT *, (SELECT usuario FROM usuarios WHERE usuarios.id = posts.id_usuario) as usuario FROM posts WHERE id_usuario ='$id' ORDER BY data_post DESC";
            $sql = $this->db->query($sql);
            if($sql->rowCount() > 0){
                $feed = $sql->fetchAll();
            }
        return $feed;
    }
    public function totalPublicacoesUsuario($id){
        $sql = "SELECT id_usuario FROM posts WHERE id_usuario = '$id'";
        $sql = $this->db->query($sql);
        return $sql->rowCount();
    }
    public function getMensagemCurtida($id_msg){
        $id = $_SESSION['twlg'];
        $sql = "SELECT * FROM curtida WHERE id_usuario = '$id' AND id_mensagem = '$id_msg'";
        $sql = $this->db->query($sql);
        if($sql->rowCount() > 0){
            return true;
        }
        return false;
    }
    public function curtirMsg($id){
        $id_usuario = $_SESSION['twlg'];
        $sql = "INSERT INTO curtida SET id_usuario = '$id_usuario', id_mensagem = '$id'";
        $sql = $this->db->query($sql);
    }
    public function descurtirMsg($id){
        $id_usuario = $_SESSION['twlg'];
        $sql = "DELETE FROM curtida WHERE id_usuario = '$id_usuario' AND id_mensagem = '$id'";
        $sql = $this->db->query($sql);
    }
    public function getCurtidorMsg($id_msg){
        $sql = "SELECT u.id, u.usuario FROM curtida c
                LEFT JOIN usuarios u ON c.id_usuario = u.id
                WHERE c.id_mensagem = '$id_msg'";
        $sql = $this->db->query($sql);
        if($sql->rowCount() > 0){
            return $sql = $sql->fetchAll();
        }else{
            return array();
        }
    }
}