<?php
class loginController extends Controller {
    /*
     * Controller responsável por gerenciar login, registro e logout
     *
     * @author Jonathan Tolotti jonathan@jonathantolotti.com.br
     *
     */
    //inicia construtor da classe
    public function __construct(){
        parent::__construct();
    }
    public function index() {
        /*
        * Metodo responsável por realizar o login do usuario e armazenar o id na session
        *
        * @param $maxTentativas - Quantidade máximas de tentativas de login, ao ultrapassar sera bloqueado a tentativa pelo tempo informado
        * @param $tempoBloqueio - Determina o tempo de bloqueio apos errar N vezes a senha, valor em segundos
        * @param $data - Array onde será inserido mensagens para exibição na view
        * @param $senha - Senha vindo do form, devidamente sanitizada
        * @param $usuario Usuário vindo do form, devidamente sanitizado
        *
        * @function fazerLogin - Recebe usuário e senha e envia ao DB para realizar login
        */
        $maxTentativas = 3;
        $tempoBloqueio = time()+30;
        $data = array('aviso' => '');
        $u = new Usuarios();
        if($u->isLogged()){
            header("Location:".BASE_URL);
        }
        if(isset($_POST['usuario']) && !empty($_POST['usuario'])){
            // inicio da validação e sanitização dos dados recebidos

            if(!empty($_POST['senha'])){
                $senha = addslashes($_POST['senha']);
                $senha = filter_var($senha, FILTER_SANITIZE_STRING | FILTER_SANITIZE_SPECIAL_CHARS);
                $senha = strip_tags($senha);
                $senha = md5($senha);
            }
            if(!empty($_POST['usuario'])){
                $usuario = addslashes($_POST['usuario']);
                $usuario = filter_var($usuario, FILTER_SANITIZE_STRING | FILTER_SANITIZE_SPECIAL_CHARS);
                $usuario = strip_tags($usuario);
            }
            if($u->fazerLogin($usuario, $senha)){
                $l = new loggerController();
                $l->registraAcesso("Acessou o sistema");
                $_SESSION['tentativas'] = 0;
                header("Location:".BASE_URL);
            }else{
                //valida tentativas de login e seta cookie caso ultrapasse
                if($_SESSION['tentativas'] >= $maxTentativas) {
                    setcookie("tentativaLogin", $_SESSION['tentativas'], $tempoBloqueio);
                }
                    $data['aviso'] = "Usuário ou senha inválido.";
            }
        }
        $this->loadView('login', $data);
    }

    public function cadastro(){
        /*
        * Metodo responsável por realizar cadastro do usuário e logar no site
        *
        * @param $data - Array onde será inserido mensagens para exibição na view
        * @param $nome - dado vindo do form, devidamente sanitizada
        * @param $senha dado vindo do form, devidamente sanitizado
        * @param $usuario dado vindo do form, devidamente sanitizado
        * @param $bio dado vindo do form, devidamente sanitizado
        * @param $foto dado vindo do form, devidamente sanitizado
        *
        * @function usuarioExiste - valida se o usuário a ser cadastrado já existe no DB
        * @function inserirUsuario - Registra novo usuário
        * @function follow - Faz o novo usuário seguir o ID 1
        */
        $u = new Usuarios();
        if($u->isLogged()){
            header("Location:".BASE_URL);
        }
        $data = array('aviso' => '');
        if(isset($_POST['usuario']) && !empty($_POST['usuario'])){

        //inicio da sanitização dos dados
        if(!empty($_POST['nome'])){
            $nome = addslashes($_POST['nome']);
            $nome = filter_var($nome, FILTER_SANITIZE_STRING | FILTER_SANITIZE_SPECIAL_CHARS);
            $nome = strip_tags($nome);
        }
        if(!empty($_POST['email'])){
            $email = filter_input(INPUT_POST,'email',FILTER_VALIDATE_EMAIL);
        }
        if(!empty($_POST['senha'])){
            $senha = addslashes($_POST['senha']);
            $senha = filter_var($senha, FILTER_SANITIZE_STRING | FILTER_SANITIZE_SPECIAL_CHARS);
            $senha = strip_tags($senha);
            $senha = md5($senha);
        }
        if(!empty($_POST['usuario'])){
            $usuario = addslashes($_POST['usuario']);
            $usuario = filter_var($usuario, FILTER_SANITIZE_STRING | FILTER_SANITIZE_SPECIAL_CHARS);
            $usuario = strip_tags($usuario);
        }
        if(!empty($_POST['bio'])){
            $bio = addslashes($_POST['bio']);
            $bio = filter_var($bio, FILTER_SANITIZE_STRING | FILTER_SANITIZE_SPECIAL_CHARS);
            $bio = strip_tags($bio);
        }
        $foto = $_FILES['foto'];
        //fim da sanitização dos dados
        if(!empty($nome) && !empty($email) && !empty($senha) && !empty($usuario)){
             if(!$u->usuarioExiste($usuario)){
                $_SESSION['twlg'] = $u->inserirUsuario($nome, $email, $senha, $usuario, $foto, $bio);
                 $r = new Relacionamentos();
                 $e = new emailController();
                 $r->follow($_SESSION['twlg'], 1);
                 $e->emailCadastro($usuario, $email);
                header("Location: ".BASE_URL);
            }else{
                $data['aviso'] = "Este usuário já existe.";
            }
        }else{
            $data['aviso'] = "Preencha todos os campos!";
        }
    }
        $this->loadView('cadastro', $data);
    }
    //realiza logout do usuario
    public function logout(){
        /*
        * Metodo responsável por realizar logout do usuário
        *
        */
        $u = new Usuarios();
        if(!$u->isLogged()){
            header("Location:".BASE_URL."login");
        }else{
            $l = new loggerController();
            $l->registraAcesso("Saiu do sistema");
            unset($_SESSION['twlg']);
            header("Location:".BASE_URL);
        }
    }
    public function gerarTokenSenha(){
        $data = [];
        if(isset($_POST['email']) && !empty($_POST['email'])){
            $u = new Usuarios();
            $email = filter_input(INPUT_POST,'email',FILTER_VALIDATE_EMAIL);
            if($u->validaEmail($email)){
                $token = md5($email.time());
                $u->gerarTokenSenha($email, $token);
                    $e = new emailController();
                    $link = BASE_URL."login/recuperarSenha/".$token."/".$email;
                    $usuario = $u->getNomeUsuarioEmail($email);
                    
                    $e->emailTokenSenha($email, $link, $usuario);
                    $data['aviso'] = "As informações de alteração de senha foram enviadas para seu e-mail.";
            }else{
                $data['aviso'] = "E-mail não localizado!";
            }
        }
        $this->loadView('esqueciSenha', $data);
    }

    public function recuperarSenha($token,$email){
        $u = new Usuarios();
        $data = [];
        if($u->validaToken($token,$email)){
            if(isset($_POST['senha']) && !empty($_POST['senha'])){
                $senha = addslashes($_POST['senha']);
                $senha = filter_var($senha, FILTER_SANITIZE_STRING | FILTER_SANITIZE_SPECIAL_CHARS);
                $senha = strip_tags($senha);
                $senha = md5($senha);
                if($u->alteraSenha($senha,$email)){
                    $u->invalidaToken($token);
                    header("Location: ".BASE_URL);
                }
            }
            $data['email'] = $email;
            $this->loadView('alterarSenha', $data);
        }else{
            echo "Token inválido!";
        }
    }
}