<?php
class usuarioController extends Controller {
    /*
     * Controller responsável por gerenciar toda parte relacionada a usuários
     *
     * @author Jonathan Tolotti jonathan@jonathantolotti.com.br
     *
     */
    //inicia construtor da classe
    public function __construct(){
        parent::__construct();
        $u = new Usuarios();

        if(!$u->isLogged()){
            header("Location:".BASE_URL."login");
        }
    }
    public function index(){
        $u = new Usuarios();
        if($u->isLogged()){
            header("Location:".BASE_URL);
        }
    }
    //carrega view perfil com id do usuario
    public function verPerfil($id){
        /*
         * Primeiro método chamado ao entrar no controlador, responsável por preencher os dados da view verPerfil
         *
         * @param data['dados'] Informações do usuário logado (ID, usuário, bio)
         * @param data['qtd_seguido'] Quantidade de usuários que seguem o id logado
         * @param data['qtd_seguidores'] Quantidade de usuários que o id logado segue
         * @param data['sugestao'] Sugestões de usuários para o id logado (excluindo o próprio id logado)
         * @param data['qtd_tweets'] Quantidade de postagens do id logado
         * @param data['feed'] Carrega o feed do id informado
         *
         */
        $p = new Post();
        $data = array(
            'dados' => ''
        );
        $u= new Usuarios($id);
        $data['dados'] = $u->getNomeUsuario($id);
        $data['qtd_seguido'] = $u->countSeguidoUsuario($id);
        $data['qtd_seguidores'] = $u->countSeguidoresUsuario($id);
        $data['sugestao'] = $u->getUsuariosPerfil($_SESSION['twlg'],10);
        $data['qtd_tweets'] = $p->totalPublicacoesUsuario($id);
        $data['feed'] = $p->getFeedUsuario($id);
        $l = new loggerController();
        $l->registraNavegacao($_SERVER['REQUEST_URI']);
        $this->loadTemplate('verPerfil', $data);
    }
    //carrega view com id do usuario
    public function verSeguidores($id){
        /*
         * Metodo responsavel por carregar os seguidores do ID informado
         *
         * @param data['dados'] Informações do usuário logado (ID, usuário, bio)
         * @param data['qtd_seguido'] Quantidade de usuários que seguem o id logado
         * @param data['qtd_seguidores'] Quantidade de usuários que o id logado segue
         * @param data['sugestao'] Sugestões de usuários para o id logado (excluindo o próprio id logado)
         * @param data['qtd_tweets'] Quantidade de postagens do id logado
         *
         */
        $p = new Post();
        $u = new Usuarios();
        $data = array(
            'dados' => ''
        );
        $data['dados'] = $u->getNomeUsuario($id);
        $data['qtd_seguido'] = $u->countSeguidoUsuario($id);
        $data['qtd_seguidores'] = $u->countSeguidoresUsuario($id);
        $data['sugestao'] = $u->getUsuariosPerfil($_SESSION['twlg'],10);
        $data['qtd_tweets'] = $p->totalPublicacoesUsuario($id);
        $data['seguidores'] = $u->verSeguidores($id);
        $l = new loggerController();
        $l->registraNavegacao($_SERVER['REQUEST_URI']);
        $this->loadTemplate('verSeguidores', $data);
    }
    public function verQuemSegue($id){
        /*
         * Metodo responsavel por carregar os quem o ID informado segue
         *
         * @param data['dados'] Informações do usuário logado (ID, usuário, bio)
         * @param data['qtd_seguido'] Quantidade de usuários que seguem o id logado
         * @param data['qtd_seguidores'] Quantidade de usuários que o id logado segue
         * @param data['sugestao'] Sugestões de usuários para o id logado (excluindo o próprio id logado)
         * @param data['qtd_tweets'] Quantidade de postagens do id logado
         * @param data['seguidores'] Seguidores do id informado
         *
         */
        $p = new Post();
        $u = new Usuarios();
        $data = array(
            'dados' => ''
        );
        $data['dados'] = $u->getNomeUsuario($id);
        $data['qtd_seguido'] = $u->countSeguidoUsuario($id);
        $data['qtd_seguidores'] = $u->countSeguidoresUsuario($id);
        $data['sugestao'] = $u->getUsuariosPerfil($_SESSION['twlg'],10);
        $data['qtd_tweets'] = $p->totalPublicacoesUsuario($id);
        $data['seguidores'] = $u->verQuemSegue($id);
        $l = new loggerController();
        $l->registraNavegacao($_SERVER['REQUEST_URI']);
        $this->loadTemplate('verQuemSegue', $data);
    }
    public function meuPerfil(){
        $data = array(
            'dados' => ''
        );
        $id = $_SESSION['twlg'];
        $u = new Usuarios();
        $data['usuario'] = $u->getNomeUsuario($id);
        $fotoAntiga = $data['usuario']['foto'];

        if(isset($_POST['usuario']) && !empty($_POST['usuario'])) {
            //inicio da sanitização dos dados
            if(!empty($_POST['nome'])) {
                $nome = addslashes($_POST['nome']);
                $nome = filter_var($nome, FILTER_SANITIZE_STRING | FILTER_SANITIZE_SPECIAL_CHARS);
                $nome = strip_tags($nome);
            }
            if(!empty($_POST['usuario'])) {
                $usuario = addslashes($_POST['usuario']);
                $usuario = filter_var($usuario, FILTER_SANITIZE_STRING | FILTER_SANITIZE_SPECIAL_CHARS);
                $usuario = strip_tags($usuario);
            }
            if(!empty($_POST['bio'])) {
                $bio = addslashes($_POST['bio']);
                $bio = filter_var($bio, FILTER_SANITIZE_STRING | FILTER_SANITIZE_SPECIAL_CHARS);
                $bio = strip_tags($bio);
            }
            if(!empty($_FILES['foto'])){
                move_uploaded_file($_FILES['foto']['tmp_name'], 'assets/images/users/' . $fotoAntiga);
            }
            $u->alterarUsuario($id, $nome, $usuario, $bio, $fotoAntiga);
                header("Location: " . BASE_URL);
        }
        $l = new loggerController();
        $l->registraNavegacao($_SERVER['REQUEST_URI']);
        $this->loadTemplate('meuPerfil', $data);
    }
    public function apagaTweet()
    {
        if(!empty($_POST['id'])) {
            $u = new Usuarios();
            $id = addslashes($_POST['id']);
            $u->apagaTweet($id);
        }
    }
}