<?php
class notFoundController extends Controller {
    /*
     * Controller responsável pela pagina 404
     *
     * @author Jonathan Tolotti jonathan@jonathantolotti.com.br
     *
     */

    public function index() {
        $data = array();
        $l = new loggerController();
        $l->registraNavegacao($_SERVER['REQUEST_URI']);
        $this->loadTemplate('404', $data);
    }

}