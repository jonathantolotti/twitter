<?php
class emailController extends Controller {
    /*
     * Controller responsável por gerenciar envio de email
     *
     * @author Jonathan Tolotti jonathan@jonathantolotti.com.br
     *
     */
    //inicia o construtor da classe e verifica se usuario esta logado
    public function __construct(){
        parent::__construct();
    }

    public function emailCadastro($usuario,$email){

        if(isset($email) && !empty($email)) {
            $email = addslashes($_POST['email']);
            $email = addslashes(filter_var($email, FILTER_VALIDATE_EMAIL));
        }
        if(isset($usuario) && !empty($usuario)) {
            $usuario = addslashes($_POST['usuario']);
            $usuario = filter_var($usuario, FILTER_SANITIZE_STRING | FILTER_SANITIZE_SPECIAL_CHARS);
        }
            $para = $email;
            $de = "jonathan@jonathantolotti.com.br";
            $assunto = "Seja bem-vindo(a), ".$usuario."!";
            $corpo = "Obrigado por registrar-se ".$usuario;
            $cabecalho = "From: ".$de."\r\n"."Reply-To: ".$email."\r\n"."X-Mailer: PHP/".phpversion();
            mail($para, $assunto, $corpo, $cabecalho);
    }
    public function emailTokenSenha($email, $link, $usuario){
            $para = $email;
            $de = "jonathan@jonathantolotti.com.br";
            $assunto = "Recuperação de senha, ".$usuario."!";
            $corpo = $usuario. ", recentemente você solicitou a alteração de sua senha, para prosseguir você deverá clicar no link: ".$link;
            $cabecalho = "From: ".$de."\r\n"."Reply-To: ".$email."\r\n"."X-Mailer: PHP/".phpversion();
            mail($para, $assunto, $corpo, $cabecalho);
    }

}