<?php
class homeController extends Controller {
    /*
     * Controller responsável por gerenciar toda parte inicial do sistema
     *
     * @author Jonathan Tolotti jonathan@jonathantolotti.com.br
     *
     */
    //inicia o construtor da classe e verifica se usuario esta logado
    public function __construct(){
        parent::__construct();
        $u = new Usuarios();

        if(!$u->isLogged()){
            header("Location:".BASE_URL."login");
        }
    }
    //responsavel pelo index do site, carrega todas informações principais
    public function index() {
        /*
         * Primeiro método chamado ao entrar no controlador, responsável por preencher os dados da view home
         *
         * @param data['dados'] Informações do usuário logado (ID, usuário, bio)
         * @param data['qtd_seguido'] Quantidade de usuários que seguem o id logado
         * @param data['qtd_seguidores'] Quantidade de usuários que o id logado segue
         * @param data['sugestao'] Sugestões de usuários para o id logado (excluindo o próprio id logado)
         * @param data['qtd_tweets'] Quantidade de postagens do id logado
         *
         * @param msg Mensagem que será enviada ao banco de dados
         */
        $p = new Post();
        $data = array(
            'dados' => ''
        );
        if(isset($_POST['msg']) && !empty($_POST['msg'])){
            $msg = addslashes(filter_input(INPUT_POST, 'msg', FILTER_SANITIZE_STRING));
            $msg = trim($msg);
            $p->publicar($msg);
        }
        $u= new Usuarios($_SESSION['twlg']);
        $data['dados'] = $u->getNome();
        $data['qtd_seguido'] = $u->countSeguido();
        $data['qtd_seguidores'] = $u->countSeguidores();
        $data['sugestao'] = $u->getUsuarios(10);
        $data['qtd_tweets'] = $p->totalPublicacoes($_SESSION['twlg']);

        $lista = $u->getSeguidos();
        $lista[] = $_SESSION['twlg'];
        $data['feed'] = $p->getFeed($lista, 10);
        $l = new loggerController();
        $l->registraNavegacao($_SERVER['REQUEST_URI']);
        $this->loadTemplate('home', $data);
    }
    public function follow(){
        /*
         * Metodo responsável por receber a requisição Ajax de follow
         *
         * @param id - ID vindo pela requisição
         *
         * @function usuarioValido - Recebe id vindo pelo Ajax e valida se existe no DB
         * @function validaFollow - Recebe id logado e id vindo pelo Ajax e valida se são diferentes (evita id logado seguir ele mesmo)
         *
         */
        $u = new Usuarios();
        if(isset($_POST['id'])){
            $id = addslashes(filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT));
            if($u->usuarioValido($id) && $u->validaFollow($_SESSION['twlg'], $id)){
                $r = new Relacionamentos();
                $r->follow($_SESSION['twlg'], $id);
            }
        }
    }
    public function unfollow(){
        /*
         * Metodo responsável por receber a requisição Ajax de unfollow
         *
         * @param id - ID vindo pela requisição
         *
         * @function usuarioValido - Recebe id vindo pelo Ajax e valida se existe no DB
         *
         */
        $u = new Usuarios();
        if(isset($_POST['id'])){
            $id = addslashes(filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT));
            if($u->usuarioValido($id)){
                $r = new Relacionamentos();
                $r->unfollow($_SESSION['twlg'], $id);
            }
        }
    }
    public function curtirMsg(){
        $p = new Post();
        if(isset($_POST['id'])){
            $id = addslashes(filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT));
            $p->curtirMsg($id);
        }
    }
    public function descurtirMsg(){
        $p = new Post();
        if(isset($_POST['id'])){
            $id = addslashes(filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT));
            $p->descurtirMsg($id);
        }
    }
}