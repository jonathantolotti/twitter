<?php
class loggerController extends Controller{
    /*
     * Classe responsável por registrar todos os logs de navegação
     */
    public function __construct(){
        parent::__construct();
        $u = new Usuarios();


        if(!$u->isLogged()){
            header("Location:".BASE_URL."login");
        }
    }
    public function registraAcesso($tipo){
        if(isset($_SESSION['twlg']) && !empty($_SESSION['twlg'])){
            $l = new Logger();
            $id_usuario = addslashes($_SESSION['twlg']);
            $user_agent = addslashes($_SERVER['HTTP_USER_AGENT']);
            $ip = addslashes($_SERVER['REMOTE_ADDR']);
            $l->registraAcesso($id_usuario,$user_agent,$ip,$tipo);
        }
    }
    public function registraNavegacao($pagina){
        if(isset($_SESSION['twlg']) && !empty($_SESSION['twlg'])){
            $l = new Logger();
            $id_usuario = addslashes($_SESSION['twlg']);
            $ip = addslashes($_SERVER['REMOTE_ADDR']);
            $l->registraNavegacao($id_usuario, $pagina, $ip);
        }
    }
}