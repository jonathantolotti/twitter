<!--
Saudade, pai
Descanse em paz
Seu filho já cresceu
Sempre quis te orgulhar
Você chegou a ver
Vou superar
Só tenho a agradecer
De todos vou cuidar
Honrar teu nome
-->
<!DOCTYPE html>
<html>
<head>
    <title>PlaceLife - Manutenção</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/ico" href="/favicon.ico"/>
    <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>assets/css/style.css">

</head>

<body class="bg-dark">
<h1 class="text-center text-light" style="font-family: 'Calibri Light'">
    Estamos em manutenção, voltamos logo ;)
</h1>
<h3 class="text-center text-light" style="font-family: 'Calibri Light'">PlaceLife</h3>

<script type="text/javascript" src="assets/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.mask.js"></script>
<script type="text/javascript" src="assets/js/script.js"></script>
</body>
</html>