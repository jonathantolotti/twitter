$(function(){
    //realiza follow via ajax
    $('.btnFollowHome').on('click', function(){
        var id = $(this).attr("id");
        $.ajax({
            url: requestURL+'home/follow',
            type:'POST',
            data:{id: id},
            success:function(){
                location.reload();
            }
        });
    });

    //realiza unfollow via ajax
    $('.btnUnfollowHome').on('click', function(){
        var id = $(this).attr("id");
        $.ajax({
            url: requestURL+'home/unfollow',
            type:'POST',
            data: {id:id},
            success:function(){
                window.location.reload();
            }
        });
    });

    //apaga tweet
    $('.btnApagaTweet').on('click', function(){
       var id = $(this).attr("id");
       $.ajax({
           url: requestURL+'usuario/apagaTweet',
           type:'POST',
           data: {id:id},
           success: function(){
                window.location.reload();
           }
       });
    });

    //curtir msg
    $('.curtirMsg').on('click', function(){
       var id = $(this).attr("id");
       $.ajax({
          url: requestURL+'home/curtirMsg',
          type:'POST',
          data:{id:id},
          success: function(){
              window.location.reload();
          }
       });
    });
    //descurtir msg
    $('.descurtirMsg').on('click', function(){
        var id = $(this).attr("id");
        $.ajax({
            url: requestURL+'home/descurtirMsg',
            type:'POST',
            data:{id:id},
            success: function(){
                window.location.reload();
            }
        });
    });



});