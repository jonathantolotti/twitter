<div class="container login" xmlns="http://www.w3.org/1999/html">
    <h1 class="display-4">Alterar dados:</h1>
    <form method="POST" enctype="multipart/form-data">
        <div class="form-group">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">@</div>
                </div>
                <input type="text" class="form-control" id="usuario" name="usuario" value="<?php echo $usuario['usuario']; ?>">
            </div>
            <input type="text" class="form-control dadosRegistro" name="nome" id="nome" value="<?php echo $usuario['nome']; ?>">
            <textarea name="bio" class="form-control" maxlength="300" rows="6" style="resize: none;"><?php echo $usuario['bio']; ?></textarea>
        </div>
        <div class="custom-file">
            <input type="file" class="custom-file-input" name="foto" id="customFile">
            <label class="custom-file-label" for="customFile">Escolha sua foto de perfil</label>
        </div>
        <div class="form-group">
            <input class="btn btn-info btn-block btnLogin" type="submit"  style="margin-top: 5px;" value="Alterar">
        </div>
    </form>
</div>


<?php
if(!empty($aviso)){
    echo '<div class="alert alert-danger" role="alert" style="text-align: center; margin:auto; width: 250px;">';
    echo $aviso;
    echo '</div>';
}
?>