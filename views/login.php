<!DOCTYPE html>
<html>
<head>
    <title>PlaceLife - Login</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css"  href="<?php echo BASE_URL; ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>assets/css/style.css">

</head>
<body>
<div class="container-fluid">
    <div class="alert alert-info alertJoni" role="alert">
        Este projeto foi criado para fins acadêmicos por <a href="https://www.jonathantolotti.com.br" class="alert-link" target="_blank">Jonathan Tolotti</a>, todas as informações são fictícias. Ao navegar neste site, você concorda que todo seu acesso está sendo registrado.
    </div>
</div>
<div class="container login">
    <h1 class="display-4">Acessar:</h1>
    <form method="POST">
        <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">@</div>
                    </div>
                    <input type="text" class="form-control" id="usuario" name="usuario" placeholder="Usuário" required maxlength="50" autofocus>
                </div>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">**</div>
                    </div>
                    <input type="password" class="form-control" id="senha" name="senha" placeholder="Senha" required maxlength="50">
                </div>
            </div>
        <?php if(!isset($_COOKIE['tentativaLogin'])): ?>
        <div class="form-group">
            <input class="btn btn-outline-info btn-block btn-sm btnLogin" type="submit" value="Acessar">
        </div>
        <?php endif; ?>
    </form>
    <a href="<?php echo BASE_URL."login/cadastro"?>" class="btn btn-primary btn-block btn-lg" style="margin-bottom: 10px;">Registre-se</a>
    <a href="<?php echo BASE_URL."login/gerarTokenSenha"?>" id="esqueciSenha">Esqueci minha senha</a>
</div>
<?php
if(!empty($aviso)){
echo '<div class="alert alert-danger" role="alert" style="text-align: center; margin:auto; width: 250px;">';
    echo $aviso;
    echo '</div>';
}
?>
</body>
<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/jquery.mask.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/script.js"></script>
</html>