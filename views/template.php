<!--
Saudade, pai
Descanse em paz
Seu filho já cresceu
Sempre quis te orgulhar
Você chegou a ver
Vou superar
Só tenho a agradecer
De todos vou cuidar
Honrar teu nome
-->
<!DOCTYPE html>
<html>
<head>
    <title>PlaceLife - Home</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/ico" href="/favicon.ico"/>
    <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>assets/css/style.css">

</head>
<body>
<div class="container-fluid">
    <div class="alert alert-info" role="alert" style=" font-family: 'Calibri Light'; padding: 5px; text-align: center; margin-top: 5px; margin-bottom: 5px;">
    Este projeto foi criado para fins acadêmicos por <a href="https://www.jonathantolotti.com.br" class="alert-link" target="_blank">Jonathan Tolotti</a>, todas as informações são fictícias. Ao navegar neste site, você concorda que todo seu acesso está sendo registrado.
    </div>
    <?php if(ENVIRONMENT == 'development'): ?>
        <div class="alert alert-danger" role="alert" style=" font-family: 'Calibri Light'; padding: 5px; text-align: center; margin-top: 5px; margin-bottom: 5px;" id="ambiente">
            <span class="ambiente"><b>ATENÇÃO!</b> Rodando em ambiente de desenvolvimento, <b>ALTERAR PARA PRODUÇÃO ANTES DO DEPLOY</b>.</span>
        </div>
    <?php endif; ?>
</div>
    <nav class="navbar navbar-expand-lg navbar-light bg-dark">
        <a class="navbar-brand text-light" href="<?php echo BASE_URL; ?>">
            <img class="" src="<?php echo BASE_URL ?>assets/images/logo.png" alt="Logo" title="Logo" height="35px;"> </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" style="background-color: white;">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="btn btn-dark" href="<?php echo BASE_URL; ?>" style="color: white;">Inicio</a>
                </li>

                <li class="nav-item">
                    <a class="btn btn-dark" href="<?php echo BASE_URL."usuario/verSeguidores/".$_SESSION['twlg']; ?>" style="color: white;">Meus seguidores</a>
                </li>

                <li class="nav-item">
                    <a class="btn btn-dark" href="<?php echo BASE_URL."usuario/meuPerfil"; ?>" style="color: white;">Meu perfil</a>
                </li>

            </ul>
         <!--   <form class="form-inline" method="POST">
                <input class="form-control mr-sm-2" type="search" placeholder="Buscar usuário" name="nome" id="busca">
                <button class="btn btn-outline-info my-2 my-sm-0" type="submit" style="margin-right: 50px;">Buscar</button>

            </form> -->
            <script>var reqSair = '<?php echo BASE_URL."login/logout"?>' </script>
            <a href="<?php echo BASE_URL."login/logout"?>" class="btn btn-dark" style="text-decoration: none; margin-right: 10px;">Sair</a>
        </div>
    </nav>
</div>
<?php $this->loadViewInTemplate($viewName, $viewData); ?>
<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/script.js"></script>
</body>
</html>