<!--<div class="d-flex flex-row bd-highlight mb-3">
    <div class="p-2 bd-highlight">MEU PERFIL</div>
    <div class="p-2 bd-highlight">TWEETS</div>
    <div class="p-2 bd-highlight">SUGESTÕES</div>
</div>-->
<!-- INICIO HOME -->
<section>
    <div class="container-fluid">
        <div class="row">
            <!-- INICIO MEU PERFIL-->
            <article class="col-sm-3 homeInfo"">
            <div class="card" style="margin-top: 5px;">
                <?php if(!empty($dados['foto'])): ?>
                    <img class="card-img-top img-thumbnail mx-auto d-block imgPerfilHome"
                         src="<?php echo BASE_URL;?>assets/images/users/<?php echo $dados['foto']; ?>">
                <?php else: ?>
                    <img class="card-img-top img-thumbnail mx-auto d-block imgPerfilHome" src="<?php echo BASE_URL;?>assets/images/users/default.png">
                <?php endif; ?>
                <div class="card-body" style="max-width: 100%;">
                    <h5 class="card-title"><a href="<?php echo BASE_URL;?>usuario/verPerfil/<?php echo $dados['id'] ?>" style="text-decoration: none;">@<?php echo
                            $dados['usuario'];?></a>
                        <?php if ($dados['verificado'] == 1): ?>
                            <img src="<?php echo BASE_URL;?>assets/images/verified.png"
                                 class="img" id="verified" title="Conta verificada"
                                 alt="Conta verificada">
                        <?php endif; ?>
                    </h5>
                    <!-- DESCRIÇÃO USUÁRIO -->
                    <span class="text-justify small"><?php echo $dados['bio']; ?></span>
                    <div class="d-flex flex-row bd-highlight mb-3">
                        <div class="p-2 bd-highlight">
                            <a href="<?php echo BASE_URL;?>usuario/verPerfil/<?php echo $dados['id']; ?>" style="text-decoration: none; color: black;">Posts:</a>
                            <a href="<?php echo BASE_URL;?>usuario/verPerfil/<?php echo $dados['id']; ?>" style="text-decoration: none;"><p class="text-center"><?php echo $qtd_tweets; ?></p></a>
                        </div>
                        <div class="p-2 bd-highlight">
                            <a href="<?php echo BASE_URL;?>usuario/verQuemSegue/<?php echo $dados['id']; ?>" style="text-decoration: none; color: black;">Seguindo:</a>
                            <a href="<?php echo BASE_URL;?>usuario/verQuemSegue/<?php echo $dados['id']; ?>" style="text-decoration: none;">
                                <p class="text-center"><?php  echo $qtd_seguido;?></p></a>
                        </div>
                        <div class="p-2 bd-highlight">
                            <a href="<?php echo BASE_URL;?>usuario/verSeguidores/<?php echo $dados['id']; ?>" style="text-decoration: none; color: black;">Seguidores:</a>
                            <a href="<?php echo BASE_URL;?>usuario/verSeguidores/<?php echo $dados['id']; ?>" style="text-decoration: none;">
                                <p class="text-center"><?php echo $qtd_seguidores; ?></p></a>
                        </div>
                    </div>
                </div>
            </div>
            </article>
            <!-- FIM MEU PERFIL -->

            <!-- INICIO -->
            <article class="col-sm-7 homeInfo">
                <!-- FEED -->
                <h5>Seguidores:</h5>
                <?php if($qtd_seguidores == 0): ?>
                    <br><h4>Este usuário não possui seguidores :(</h4>
                <?php endif; ?>
                <table border="0" width="100%">
                    <tr>
                        <td width="80%"></td>
                        <td></td>
                    </tr>
                    <?php foreach($seguidores as $usuario): ?>
                        <script>
                            var requestURL = '<?php echo BASE_URL; ?>'
                        </script>
                        <tr>
                            <td><a href="<?php echo BASE_URL;?>usuario/verPerfil/<?php echo $usuario['id'] ?>" style="text-decoration: none;">@<?php echo $usuario['usuario']; ?></a></td>
                            <td>
                                <?php if($usuario['seguido'] == '0'): ?>

                                    <button class="btn badge badge-info btnFollowHome"
                                            id="<?php echo $usuario['id'];?>" <?php echo $usuario['id'] == $_SESSION['twlg']?'disabled':'';?>>Follow</button>
                                <?php else: ?>
                                    <button class="btn badge badge-secondary btnUnfollowHome" id="<?php echo $usuario['id'];?>">Unfollow</button>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </article>
            <!-- FIM -->

            <!-- INICIO SUGESTÕES -->
            <article class="col-sm-2 homeInfo">
                <h5>Sugestões:</h5><br>
                <table border="0" width="100%" class="sugestao">
                    <tr>
                        <td width="80%"></td>
                        <td></td>
                    </tr>
                    <?php foreach($sugestao as $usuario): ?>
                        <script>
                            var requestURL = '<?php echo BASE_URL; ?>'
                        </script>
                        <tr>
                            <td><a href="<?php echo BASE_URL;?>usuario/verPerfil/<?php echo $usuario['id'] ?>" style="text-decoration: none;">@<?php echo $usuario['usuario']; ?></a></td>
                            <td>
                                <?php if($usuario['seguido'] == '0'): ?>
                                    <button class="btn badge badge-info btnFollowHome" id="<?php echo $usuario['id'];?>">Follow</button>
                                <?php else: ?>
                                    <button class="btn badge badge-secondary btnUnfollowHome" id="<?php echo $usuario['id'];?>">Unfollow</button>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </article>
            <!-- FIM SUGESTÕES -->
        </div>
    </div>
</section>
<!-- FIM HOME -->
