<!-- INICIO HOME -->
<section class="bg-light">
    <div class="container-fluid">
        <div class="row">
            <!-- INICIO MEU PERFIL-->
            <article class="col-sm-3 homeInfo"
            ">
            <div class="card" style="margin-top: 5px;">
                <?php if(!empty($dados['foto'])): ?>
                    <img class="card-img-top img-thumbnail mx-auto d-block imgPerfilHome img-fluid"
                         src="<?php echo BASE_URL; ?>assets/images/users/<?php echo $dados['foto']; ?>">
                <?php else: ?>
                    <img class="card-img-top img-thumbnail mx-auto d-block imgPerfilHome"
                         src="<?php echo BASE_URL; ?>assets/images/users/default.png">
                <?php endif; ?>
                <div class="card-body" style="max-width: 100%;">
                    <h5 class="card-title"><a
                                href="<?php echo BASE_URL; ?>usuario/verPerfil/<?php echo $dados['id']; ?>"
                                style="text-decoration: none;">@<?php echo $dados['usuario']; ?></a>
                        <?php if ($dados['verificado'] == 1): ?>
                                <img src="<?php echo BASE_URL;?>assets/images/verified.png"
                                     class="img" id="verified" title="Conta verificada"
                                     alt="Conta verificada">
                        <?php endif; ?>
                    </h5>
                    <!-- DESCRIÇÃO USUÁRIO -->
                    <span class="text-justify small"><?php echo $dados['bio']; ?></span>
                    <div class="d-flex flex-row bd-highlight mb-3">
                        <div class="p-2 bd-highlight">
                            <a href="<?php echo BASE_URL; ?>usuario/verPerfil/<?php echo $dados['id']; ?>"
                               style="text-decoration: none; color: black;">Posts:</a>
                            <a href="<?php echo BASE_URL; ?>usuario/verPerfil/<?php echo $dados['id']; ?>"
                               style="text-decoration: none;">
                                <p class="text-center"><?php echo $qtd_tweets; ?></p></a>
                        </div>
                        <div class="p-2 bd-highlight">
                            <a href="<?php echo BASE_URL; ?>usuario/verQuemSegue/<?php echo $dados['id']; ?>"
                               style="text-decoration: none; color: black;">Seguindo:</a>
                            <a href="<?php echo BASE_URL; ?>usuario/verQuemSegue/<?php echo $dados['id']; ?>"
                               style="text-decoration: none;">
                                <p class="text-center"><?php echo $qtd_seguido; ?></p></a>
                        </div>
                        <div class="p-2 bd-highlight">
                            <a href="<?php echo BASE_URL; ?>usuario/verSeguidores/<?php echo $dados['id']; ?>"
                               style="text-decoration: none; color: black;">Seguidores:</a>
                            <a href="<?php echo BASE_URL; ?>usuario/verSeguidores/<?php echo $dados['id']; ?>"
                               style="text-decoration: none;">
                                <p class="text-center"><?php echo $qtd_seguidores; ?></p></a>
                        </div>
                    </div>
                </div>
            </div>
            </article>
            <!-- FIM MEU PERFIL -->

            <!-- INICIO TIMELINE -->
            <article class="col-sm-7 homeInfo">
                <!-- PUBLICAR -->
                <form method="post">
                    <textarea name="msg" class="form-control" style="margin-top: 5px; resize: none"
                              placeholder="O que você está pensando agora?" rows="3"
                              maxlength="240"></textarea>
                    <input type="submit" value="Publicar" class="btn btn-dark btn-lg btn-block"
                           style="margin-top: 5px; margin-bottom: 5px;">
                </form>
                <!-- FEED -->
                <h5>Últimas publicações:</h5>
                <?php foreach($feed as $item):?>
                    <!-- Modal  MODAL VEM PRIMEIRO POIS É CHAMADO PELO ID DA MENSAGEM-->
                    <div class="modal fade" id="<?php echo $item['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="23" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="modalCurtidas">Quem curtiu:</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <table border="0" width="100%" class="sugestao">
                                        <tr>
                                            <td width="100%"></td>
                                        </tr>
                                        <?php  $p = new Post();
                                        foreach($p->getCurtidorMsg($item['id']) as $usuario):?>
                                            <script>
                                                var requestURL = '<?php echo BASE_URL; ?>'
                                            </script>
                                            <tr>
                                                <td>
                                                    <a href="<?php echo BASE_URL; ?>usuario/verPerfil/<?php echo $usuario['id'] ?>"
                                                       style="text-decoration: none;">@<?php echo $usuario['usuario']; ?>
                                                </td>
                                                </a>
                                            </tr>
                                        <?php endforeach; ?>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card" style="margin-bottom: 5px;">
                        <div class="card-header">
                            <a href="<?php echo BASE_URL; ?>usuario/verPerfil/<?php echo $item['id_usuario']; ?>"
                               style="text-decoration: none; color: <?php echo $item['id_usuario'] == 1 ? 'black' : ''; ?>;">
                                <b>@<?php echo $item['usuario']; ?></a></b>
                        </div>
                        <div class="card-body">
                            <script>
                                var requestURL = '<?php echo BASE_URL; ?>'
                            </script>
                            <blockquote class="blockquote mb-0">
                                <p><?php echo $item['mensagem']; ?></p>
                                <footer class="blockquote-footer"><?php echo date('d/m/y H:m', strtotime($item['data_post'])); ?>
                                    <?php

                                    if(!$p->getMensagemCurtida($item['id'])):
                                        ?>
                                        <a id="<?php echo $item['id'] ?>" class="curtirMsg"><img
                                                    src="<?php echo BASE_URL; ?>assets/images/like.png"
                                                    width="20" height="20"></a>
                                    <?php else: ?>
                                        <a id="<?php echo $item['id'] ?>" class="descurtirMsg"><img
                                                    src="<?php echo BASE_URL; ?>assets/images/deslike.png"
                                                    width="20" height="20"></a>
                                    <?php endif; ?>
                                    <span data-toggle="modal" data-target="#<?php echo $item['id'];?>" style="cursor: pointer;"><?php echo $item['curtidas']; ?> curtidas</span>
                                </footer>
                            </blockquote>
                        </div>
                    </div>
                <?php endforeach; ?>
            </article>
            <!-- FIM TIMELINE -->

            <!-- INICIO SUGESTÕES -->
            <article class="col-sm-2 homeInfo">
                <h5>Sugestões:</h5><br>
                <table border="0" width="100%" class="sugestao">
                    <tr>
                        <td width="80%"></td>
                    </tr>
                    <?php foreach($sugestao as $usuario): ?>
                        <script>
                            var requestURL = '<?php echo BASE_URL; ?>'
                        </script>
                        <tr>
                            <td>
                                <a href="<?php echo BASE_URL; ?>usuario/verPerfil/<?php echo $usuario['id'] ?>"
                                   style="text-decoration: none;">@<?php echo $usuario['usuario']; ?>
                            </td>
                            </a>
                            <td>
                                <?php if($usuario['seguido'] == '0'): ?>
                                    <button class="btn badge badge-info btnFollowHome"
                                            id="<?php echo $usuario['id']; ?>">Follow
                                    </button>
                                <?php else: ?>
                                    <a href="<?php echo BASE_URL . "home/unfollow/" . $usuario['id']; ?>"
                                       class="badge badge-secondary">Unfollow</a>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </article>
            <!-- FIM SUGESTÕES -->
        </div>
    </div>
</section>
<!-- FIM HOME -->