<!DOCTYPE html>
<html>
<head>
    <title>Projeto Twitter - Jonathan Tolotti</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>assets/css/style.css">
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/jquery.mask.js"></script>
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/script.js"></script>
</head>
<body>
<div class="container-fluid">
    <div class="alert alert-info" role="alert" style=" font-family: 'Calibri Light'; padding: 5px; text-align: center; margin-top: 5px; margin-bottom: 5px;">
        Este projeto foi criado para fins acadêmicos por <a href="https://www.jonathantolotti.com.br" class="alert-link" target="_blank">Jonathan Tolotti</a>, todas as informações são fictícias. Ao navegar neste site, você concorda que todo seu acesso está sendo registrado.
    </div>
</div>
<div class="container esqueciSenha">
    <h1>Recupere sua senha:</h1>
    <form method="POST">
        <div class="form-group">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">@</div>
                </div>
                <input type="text" class="form-control" id="email" name="email" placeholder="Digite seu e-mail" required>
            </div>
        <div class="form-group">
            <input class="btn btn-info btn-block btnEsqueciSenha" type="submit" value="Enviar">
        </div>
    </form>
    <a href="<?php echo BASE_URL."login"?>" class="btn btn-outline-primary btn-block btn-lg" style="margin-bottom: 10px;">Acessar</a>
</div>
<?php
if(!empty($aviso)){
    echo '<div class="alert alert-danger" role="alert" style="text-align: center; margin:auto; width: 250px;">';
    echo $aviso;
    echo '</div>';
}
?>

</body>
</html>
