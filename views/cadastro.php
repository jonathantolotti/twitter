<!DOCTYPE html>
<html>
<head>
    <title>Projeto Twitter - Jonathan Tolotti</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>assets/css/style.css">
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/jquery.mask.js"></script>
    <script type="text/javascript" src="<?php echo BASE_URL; ?>assets/js/script.js"></script>
</head>
<body>
<div class="container-fluid">
    <div class="alert alert-info" role="alert" style=" font-family: 'Calibri Light'; padding: 5px; text-align: center; margin-top: 5px; margin-bottom: 5px;">
        Este projeto foi criado para fins acadêmicos por <a href="https://www.jonathantolotti.com.br" class="alert-link" target="_blank">Jonathan Tolotti</a>, todas as informações são fictícias. Ao navegar neste site, você concorda que todo seu acesso está sendo registrado.
    </div>
</div>
<div class="container login">
    <h1 class="display-4">Registrar-se:</h1>
    <form method="POST" enctype="multipart/form-data">
        <div class="form-group">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">@</div>
                </div>
                <input type="text" class="form-control" id="usuario" name="usuario" placeholder="Usuário" required>
            </div>
            <input type="password" class="form-control dadosRegistro" placeholder="Digite a senha" name="senha" id="senha" required>
            <input type="email" class="form-control dadosRegistro" placeholder="Digite o e-mail" name="email" id="email" required>
            <input type="text" class="form-control dadosRegistro" placeholder="Digite seu nome" name="nome" id="nome" required>
            <textarea placeholder="Descreva você em 300 caracteres ;)" name="bio" class="form-control" maxlength="300" rows="6" style="resize: none;"></textarea>
            <div class="custom-file" style="margin-top: 5px;">
                <input type="file" class="custom-file-input" name="foto" id="customFile" required>
                <label class="custom-file-label" for="customFile">Escolha sua foto de perfil</label>
            </div>
        </div>
        <div class="form-group">
            <input class="btn btn-info btn-block btnLogin" type="submit" value="Registrar">
        </div>
    </form>
    <a href="<?php echo BASE_URL."login"?>" class="btn btn-outline-primary btn-block btn-lg" style="margin-bottom: 10px;">Acessar</a>
</div>

<?php
if(!empty($aviso)){
    echo '<div class="alert alert-danger" role="alert" style="text-align: center; margin:auto; width: 250px;">';
    echo $aviso;
    echo '</div>';
}
?>

</body>
</html>