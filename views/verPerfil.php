<!--<div class="d-flex flex-row bd-highlight mb-3">
    <div class="p-2 bd-highlight">MEU PERFIL</div>
    <div class="p-2 bd-highlight">TWEETS</div>
    <div class="p-2 bd-highlight">SUGESTÕES</div>
</div>-->
<!-- INICIO HOME -->
<section>
    <div class="container-fluid">
        <div class="row">
            <!-- INICIO MEU PERFIL-->
            <article class="col-sm-3 homeInfo"">
            <div class="card" style="margin-top: 5px;">
                <?php if(!empty($dados['foto'])): ?>
                    <img class="card-img-top img-thumbnail mx-auto d-block imgPerfilHome"
                         src="<?php echo BASE_URL;?>assets/images/users/<?php echo $dados['foto']; ?>">
                <?php else: ?>
                    <img class="card-img-top img-thumbnail mx-auto d-block imgPerfilHome" src="<?php echo BASE_URL;?>assets/images/users/default.png">
                <?php endif; ?>
                <div class="card-body" style="max-width: 100%;">
                    <h5 class="card-title"><a href="<?php echo BASE_URL;?>usuario/verPerfil/<?php echo $dados['id'] ?>" style="text-decoration: none;">@<?php echo
                            $dados['usuario'];?></a>
                        <?php if ($dados['verificado'] == 1): ?>
                            <img src="<?php echo BASE_URL;?>assets/images/verified.png"
                                 class="img" id="verified" title="Conta verificada"
                                 alt="Conta verificada">
                        <?php endif; ?>
                    </h5>
                    <!-- DESCRIÇÃO USUÁRIO -->
                    <span class="text-justify small"><?php echo $dados['bio']; ?></span>
                    <div class="d-flex flex-row bd-highlight mb-3">
                        <div class="p-2 bd-highlight">
                            <a href="<?php echo BASE_URL;?>usuario/verPerfil/<?php echo $dados['id']; ?>" style="text-decoration: none; color: black;">Posts:</a>
                            <a href="<?php echo BASE_URL;?>usuario/verPerfil/<?php echo $dados['id']; ?>" style="text-decoration: none;"><p class="text-center"><?php echo $qtd_tweets; ?></p></a>
                        </div>
                        <div class="p-2 bd-highlight">
                            <a href="<?php echo BASE_URL;?>usuario/verQuemSegue/<?php echo $dados['id']; ?>" style="text-decoration: none; color: black;">Seguindo:</a>
                            <a href="<?php echo BASE_URL;?>usuario/verQuemSegue/<?php echo $dados['id']; ?>" style="text-decoration: none;">
                                <p class="text-center"><?php  echo $qtd_seguido;?></p></a>
                        </div>
                        <div class="p-2 bd-highlight">
                            <a href="<?php echo BASE_URL;?>usuario/verSeguidores/<?php echo $dados['id']; ?>" style="text-decoration: none; color: black;">Seguidores:</a>
                            <a href="<?php echo BASE_URL;?>usuario/verSeguidores/<?php echo $dados['id']; ?>" style="text-decoration: none;">
                                <p class="text-center"><?php echo $qtd_seguidores; ?></p></a>
                        </div>
                    </div>
                </div>
            </div>
            </article>
            <!-- FIM MEU PERFIL -->

            <!-- INICIO TIMELINE -->
            <article class="col-sm-7 homeInfo">
                <!-- FEED -->
                <h5>Últimas publicações:</h5>
                <script>
                    var requestURL = '<?php echo BASE_URL; ?>'
                </script>
                <?php if(empty($feed)): ?>
                <br><h4>Não existem publicações ainda :(</h4>
                <?php endif; ?>
                <?php foreach($feed as $item): ?>
                    <div class="card">
                        <div class="card-header">
                            <a href="<?php echo BASE_URL; ?>usuario/verPerfil/<?php echo $item['id_usuario']; ?>"
                               style="text-decoration: none; color: <?php echo $item['id_usuario']==1?'black':''; ?>;">
                                <b>@<?php echo $item['usuario']; ?></a></b>
                            <?php if($item['id_usuario'] == $_SESSION['twlg']): ?>
                            <button class="btn badge badge-dark btnApagaTweet" id="<?php echo $item['id'];?>" style="float: right;">X</button>
                            <?php endif; ?>
                        </div>
                        <div class="card-body">
                            <blockquote class="blockquote mb-0">
                                <p><?php echo $item['mensagem']; ?></p>
                                <footer class="blockquote-footer"><?php echo date('d/m/y H:m', strtotime($item['data_post'])); ?></footer>
                            </blockquote>
                        </div>
                    </div>
                <?php endforeach; ?>
            </article>
            <!-- FIM TIMELINE -->

            <!-- INICIO SUGESTÕES -->
            <article class="col-sm-2 homeInfo">
                <h5>Sugestões:</h5><br>
                <table border="0" width="100%" class="sugestao">
                    <tr>
                        <td width="80%"></td>
                        <td></td>
                    </tr>
                    <?php foreach($sugestao as $usuario): ?>
                        <script>
                            var requestURL = '<?php echo BASE_URL; ?>'
                        </script>
                        <tr>
                            <td><a href="<?php echo BASE_URL;?>usuario/verPerfil/<?php echo $usuario['id'] ?>" style="text-decoration: none;">@<?php echo $usuario['usuario']; ?></a></td>
                            <td>
                                <?php if($usuario['seguido'] == '0'): ?>
                                    <button class="btn badge badge-info btnFollowHome" id="<?php echo $usuario['id'];?>">Follow</button>
                                <?php else: ?>
                                    <a href="<?php echo BASE_URL."home/unfollow/".$usuario['id']; ?>" class="badge badge-secondary">Unfollow</a>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </article>
            <!-- FIM SUGESTÕES -->
        </div>
    </div>
</section>
<!-- FIM HOME -->
